package com.backend.store.controller;

import com.backend.store.dto.request.AttributeRequest;
import com.backend.store.dto.response.AttributeResponse;
import com.backend.store.entity.AttributeEntity;
import com.backend.store.mapper.AttributeMapper;
import com.backend.store.service.AttributeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@PreAuthorize("hasAnyAuthority('ADMIN')")
@RestController
@RequestMapping(value = "/api/attributes", produces = MediaType.APPLICATION_JSON_VALUE)
public class AttributeController {
    private final AttributeService attributeService;
    private final AttributeMapper attributeMapper;

    @Autowired
    public AttributeController(AttributeService attributeService, AttributeMapper attributeMapper) {
        this.attributeService = attributeService;
        this.attributeMapper = attributeMapper;
    }

    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<AttributeResponse> getAttributeById(@PathVariable("id") Long id) {
        AttributeResponse response = attributeMapper.AttributeEntityToResponse(attributeService.getAttributeById(id));
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<List<AttributeResponse>> getAllAttributes() {
        List<AttributeEntity> attributeEntities = attributeService.getAllAttributes();
        List<AttributeResponse> response = attributeEntities.stream()
                .map(attributeMapper::AttributeEntityToResponse)
                .collect(Collectors.toList());
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<AttributeResponse> createAttribute(@RequestBody AttributeRequest attributeRequest) {
        AttributeEntity attributeEntity = attributeMapper.AttributeRequestToEntity(attributeRequest);
        AttributeEntity createdAttribute = attributeService.createAttribute(attributeEntity);
        AttributeResponse response = attributeMapper.AttributeEntityToResponse(createdAttribute);
        return new ResponseEntity<>(response, HttpStatus.CREATED);
    }

    @PutMapping(value = "/{id}")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<AttributeResponse> updateAttribute(@PathVariable("id") Long id, @RequestBody AttributeRequest attributeRequest) {
        AttributeEntity attributeEntity = attributeMapper.AttributeRequestToEntity(attributeRequest);
        AttributeEntity updatedAttribute = attributeService.updateAttribute(id, attributeEntity);
        AttributeResponse response = attributeMapper.AttributeEntityToResponse(updatedAttribute);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteAttribute(@PathVariable("id") Long id) {
        attributeService.deleteAttribute(id);
    }

}
