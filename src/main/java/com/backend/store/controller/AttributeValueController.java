package com.backend.store.controller;

import com.backend.store.dto.request.AttributeValueRequest;
import com.backend.store.dto.response.AttributeValueResponse;
import com.backend.store.entity.AttributeValueEntity;
import com.backend.store.mapper.AttributeValueMapper;
import com.backend.store.service.AttributeValueService;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping(value = "/api/attribute-values", produces = MediaType.APPLICATION_JSON_VALUE)
public class AttributeValueController {
    private final AttributeValueService attributeValueService;
    private final AttributeValueMapper attributeValueMapper;

    public AttributeValueController(AttributeValueService attributeValueService, AttributeValueMapper attributeValueMapper) {
        this.attributeValueService = attributeValueService;
        this.attributeValueMapper = attributeValueMapper;
    }

    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<AttributeValueResponse> getAttributeValueById(@PathVariable("id") Long id) {
        AttributeValueResponse response = attributeValueMapper.AttributeValueEntityToResponse(attributeValueService.getAttributeValueById(id));
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<List<AttributeValueResponse>> getAllAttributes() {
        List<AttributeValueEntity> attributeValueEntities = attributeValueService.getAllAttributeValues();
        List<AttributeValueResponse> response = attributeValueEntities.stream()
                .map(attributeValueMapper::AttributeValueEntityToResponse)
                .collect(Collectors.toList());
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @PostMapping
    @PreAuthorize("hasAnyAuthority('ADMIN')")
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<AttributeValueResponse> createAttributeValue(@RequestBody AttributeValueRequest attributeValueRequest) {
        AttributeValueEntity attributeValueEntity = attributeValueMapper.AttributeValueRequestToEntity(attributeValueRequest);
        AttributeValueEntity createdAttributeValueEntity = attributeValueService.createAttributeValue(attributeValueEntity);
        AttributeValueResponse response = attributeValueMapper.AttributeValueEntityToResponse(createdAttributeValueEntity);
        return new ResponseEntity<>(response, HttpStatus.CREATED);
    }

    @PutMapping(value = "/{id}")
    @PreAuthorize("hasAnyAuthority('ADMIN')")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<AttributeValueResponse> updateAttributeValue(@PathVariable("id") Long id, @RequestBody AttributeValueRequest attributeValueRequest) {
        AttributeValueEntity attributeValueEntity = attributeValueMapper.AttributeValueRequestToEntity(attributeValueRequest);
        AttributeValueEntity updatedAttributeValueEntity = attributeValueService.updateAttributeValue(id, attributeValueEntity);
        AttributeValueResponse response = attributeValueMapper.AttributeValueEntityToResponse(updatedAttributeValueEntity);
        return new ResponseEntity<>(response, HttpStatus.OK);
    }


    @DeleteMapping("/{id}")
    @PreAuthorize("hasAnyAuthority('ADMIN')")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteAttributeValue(@PathVariable("id") Long id) {
        attributeValueService.deleteAttributeValue(id);
    }


}
