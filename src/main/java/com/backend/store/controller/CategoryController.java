package com.backend.store.controller;

import com.backend.store.dto.request.CategoryRequest;
import com.backend.store.dto.response.CategoryListResponse;
import com.backend.store.dto.response.CategoryResponse;
import com.backend.store.entity.CategoryEntity;
import com.backend.store.mapper.CategoryMapper;
import com.backend.store.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping(value = "/api/categories", produces = MediaType.APPLICATION_JSON_VALUE)
public class CategoryController {
    private final CategoryService categoryService;
    private final CategoryMapper categoryMapper;

    @Autowired
    public CategoryController(CategoryService categoryService, CategoryMapper categoryMapper) {
        this.categoryService = categoryService;
        this.categoryMapper = categoryMapper;
    }
    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<CategoryResponse> getCategoryById(@PathVariable("id") Long id) {
        CategoryResponse response = categoryMapper.categoryEntityToResponse(categoryService.getCategoryById(id));
        return new ResponseEntity<>(response, HttpStatus.OK);
    }
    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<List<CategoryListResponse>> getAllCategories() {
        List<CategoryEntity> categoryEntities = categoryService.getAllCategories();
        List<CategoryListResponse> response = categoryEntities.stream()
                .map(categoryMapper::categoryEntityToResponseList)
                .collect(Collectors.toList());
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @PostMapping
    @PreAuthorize("hasAnyAuthority('ADMIN')")
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<CategoryResponse> createCategory(@RequestBody CategoryRequest categoryRequest) {
        CategoryEntity categoryEntity = categoryMapper.categoryRequestToEntity(categoryRequest);
        CategoryEntity createdCategoryEntity = categoryService.createCategory(categoryEntity);
        CategoryResponse categoryResponse = categoryMapper.categoryEntityToResponse(createdCategoryEntity);
        return new ResponseEntity<>(categoryResponse, HttpStatus.CREATED);
    }

    @PutMapping(value = "/{id}")
    @PreAuthorize("hasAnyAuthority('ADMIN')")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<CategoryResponse> updateCategory(@PathVariable("id") Long id, @RequestBody CategoryRequest categoryRequest) {
        CategoryEntity categoryEntity = categoryMapper.categoryRequestToEntity(categoryRequest);
        CategoryEntity updatedCategoryEntity = categoryService.updateCategory(id, categoryEntity);
        CategoryResponse categoryResponse = categoryMapper.categoryEntityToResponse(updatedCategoryEntity);
        return new ResponseEntity<>(categoryResponse, HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteCategory(@PathVariable("id") Long id) {
        categoryService.deleteCategory(id);
    }
}
