package com.backend.store.controller;

import com.backend.store.dto.request.OrderRequest;
import com.backend.store.dto.response.OrderResponse;
import com.backend.store.entity.OrderEntity;
import com.backend.store.mapper.OrderMapper;
import com.backend.store.service.OrderService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping(value = "/api/orders", produces = MediaType.APPLICATION_JSON_VALUE)
public class OrderController {
    private final OrderService orderService;
    private final OrderMapper orderMapper;
    @Autowired
    public OrderController(OrderService orderService, OrderMapper orderMapper) {
        this.orderService = orderService;
        this.orderMapper = orderMapper;
    }
    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<OrderResponse> getOrderById(@PathVariable("id") Long id) {
        OrderResponse response = orderMapper.orderEntityToResponse(orderService.getOrderById(id));
        return new ResponseEntity<>(response, HttpStatus.OK);
    }
    @GetMapping
    @PreAuthorize("hasAnyAuthority('ADMIN')")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity <List<OrderResponse>> getOrders() {
        List<OrderEntity> orderEntities = orderService.getAllOrders();
        List<OrderResponse> response = orderEntities.stream()
                .map(orderMapper::orderEntityToResponse)
                .collect(Collectors.toList());
        return new ResponseEntity<>(response, HttpStatus.OK);
    }
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<OrderResponse> createOrder(@RequestBody OrderRequest orderRequest) {
        OrderEntity orderEntity = orderMapper.orderRequestToEntity(orderRequest);
        OrderEntity createdOrderEntity = orderService.createOrder(orderEntity);
        OrderResponse orderResponse = orderMapper.orderEntityToResponse(createdOrderEntity);
        return new ResponseEntity<>(orderResponse, HttpStatus.CREATED);
    }

    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteOrder(@PathVariable("id") Long id) {
        orderService.deleteOrder(id);
    }

    @PostMapping("/{orderId}/processPayment")
    public ResponseEntity<String> processPayment(
            @PathVariable Long orderId,
            @RequestParam String token) {
        try {
            String paymentResult = orderService.processPayment(orderId, token);
            return ResponseEntity.ok(paymentResult);
        } catch (Exception e) {
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).body("Internal Server Error");
        }
    }
}
