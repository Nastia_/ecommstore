package com.backend.store.controller;

import com.backend.store.dto.request.OrderItemRequest;
import com.backend.store.dto.request.ProductRequest;
import com.backend.store.dto.response.OrderItemResponse;
import com.backend.store.dto.response.OrderResponse;
import com.backend.store.dto.response.ProductResponse;
import com.backend.store.entity.OrderEntity;
import com.backend.store.entity.OrderItemEntity;
import com.backend.store.entity.ProductEntity;
import com.backend.store.mapper.OrderItemMapper;
import com.backend.store.mapper.OrderMapper;
import com.backend.store.service.OrderItemService;
import com.backend.store.service.OrderService;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping(value = "/api/order-items", produces = MediaType.APPLICATION_JSON_VALUE)
public class OrderItemController {
    private final OrderItemService orderItemService;
    private final OrderItemMapper orderItemMapper;

    public OrderItemController(OrderItemService orderItemService, OrderItemMapper orderItemMapper) {
        this.orderItemService = orderItemService;
        this.orderItemMapper = orderItemMapper;
    }
    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<OrderItemResponse> getOrderById(@PathVariable("id") Long id) {
        OrderItemResponse response = orderItemMapper.orderItemEntityToResponse(orderItemService.getOrderItemEntityById(id));
        return new ResponseEntity<>(response, HttpStatus.OK);
    }
    @GetMapping
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity <List<OrderItemResponse>> getOrders() {
        List<OrderItemEntity> orderItemEntities = orderItemService.getAllOrderItems();
        List<OrderItemResponse> response = orderItemEntities.stream()
                .map(orderItemMapper::orderItemEntityToResponse)
                .collect(Collectors.toList());
        return new ResponseEntity<>(response, HttpStatus.OK);
    }
    @PostMapping
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<OrderItemResponse> createOrderItem(@RequestBody OrderItemRequest orderItemRequest) {
        OrderItemEntity orderItemEntity = orderItemMapper.orderItemRequestToEntity(orderItemRequest);
        OrderItemEntity createdOrderItemEntity = orderItemService.createOrderItem(orderItemEntity);
        OrderItemResponse orderItemResponse = orderItemMapper.orderItemEntityToResponse(createdOrderItemEntity);
        return new ResponseEntity<>(orderItemResponse, HttpStatus.CREATED);
    }

    @PutMapping(value = "/{id}")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<OrderItemResponse> updateOrderItem(@PathVariable("id") Long id, @RequestBody OrderItemRequest orderItemRequest) {
        OrderItemEntity orderItemEntity = orderItemMapper.orderItemRequestToEntity(orderItemRequest);
        OrderItemEntity updatedOrderItemEntity = orderItemService.updateOrderItem(id, orderItemEntity);
        OrderItemResponse orderItemResponse = orderItemMapper.orderItemEntityToResponse(updatedOrderItemEntity);
        return new ResponseEntity<>(orderItemResponse, HttpStatus.OK);
    }
    @DeleteMapping("/{id}")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteOrderItem(@PathVariable("id") Long id) {
        orderItemService.deleteOrderItem(id);
    }
}
