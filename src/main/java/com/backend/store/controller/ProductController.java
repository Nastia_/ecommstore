package com.backend.store.controller;

import com.backend.store.dto.request.ProductRequest;
import com.backend.store.dto.response.ProductResponse;
import com.backend.store.entity.ProductEntity;
import com.backend.store.mapper.ProductMapper;
import com.backend.store.service.ProductService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Collectors;

@RestController
@RequestMapping(value = "/api/products", produces = MediaType.APPLICATION_JSON_VALUE)
public class ProductController {
    private final ProductService productService;
    private final ProductMapper productMapper;

    @Autowired
    public ProductController(ProductService productService, ProductMapper productMapper) {
        this.productService = productService;
        this.productMapper = productMapper;
    }

    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<ProductResponse> getCategoryById(@PathVariable("id") Long id) {
        ProductResponse response = productMapper.productEntityToResponse(productService.getProductById(id));
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @GetMapping()
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<List<ProductResponse>> getAllProducts() {
        List<ProductEntity> productEntities = productService.getAllProducts();
        List<ProductResponse> response = productEntities.stream()
                .map(productMapper::productEntityToResponse)
                .collect(Collectors.toList());
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @GetMapping(path = "/", params = {"page", "size", "title"})
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<List<ProductResponse>> getByTitle(
            @RequestParam(value = "title") String partialTitle,
            @RequestParam(value = "page", defaultValue = "1") int pageNumber,
            @RequestParam(value = "size", defaultValue = "10") int pageSize) {
        List<ProductEntity> productEntities = productService.getProductsByPartialTitle(pageNumber, pageSize, partialTitle);
        List<ProductResponse> response = productEntities.stream()
                .map(productMapper::productEntityToResponse)
                .collect(Collectors.toList());
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @GetMapping(path = "/", params = {"page", "size"})
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<List<ProductResponse>> getAllProductsPageable(
            @RequestParam(value = "page", defaultValue = "1") int pageNumber,
            @RequestParam(value = "size", defaultValue = "10") int pageSize
    ) {
        if (pageNumber < 0 || pageSize < 1) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        List<ProductEntity> productEntities = productService.getProductsByPagination(pageNumber, pageSize);
        List<ProductResponse> response = productEntities.stream()
                .map(productMapper::productEntityToResponse)
                .collect(Collectors.toList());
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @GetMapping(path = "/", params = {"page", "size", "category"})
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<List<ProductResponse>> getAllProductsByCategoryPageable(
            @RequestParam(value = "page", defaultValue = "1") int pageNumber,
            @RequestParam(value = "size", defaultValue = "10") int pageSize,
            @RequestParam(value = "category", defaultValue = "10") Long categoryId
    ) {
        if (pageNumber < 0 || pageSize < 1) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
        List<ProductEntity> productEntities = productService.getProductsByCategoryAndPagination(pageNumber, pageSize, categoryId);
        List<ProductResponse> response = productEntities.stream()
                .map(productMapper::productEntityToResponse)
                .collect(Collectors.toList());
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @GetMapping(path = "/", params = {"page", "size", "title", "sort"})
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<List<ProductResponse>> getByTitleSortAndParameter(
            @RequestParam(value = "title", required = false) String partialTitle,
            @RequestParam(value = "page", defaultValue = "1") int pageNumber,
            @RequestParam(value = "size", defaultValue = "10") int pageSize,
            @RequestParam(value = "sort", defaultValue = "asc") String sortDirection) {

        Sort.Direction sortDir = Sort.Direction.fromString(sortDirection);
        if (sortDir == null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        List<ProductEntity> productEntities = productService.getProductsByPartialTitleAndSort(pageNumber, pageSize, partialTitle, sortDir);
        List<ProductResponse> response = productEntities.stream()
                .map(productMapper::productEntityToResponse)
                .collect(Collectors.toList());

        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @GetMapping(path = "/", params = {"page", "size", "sort"})
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<List<ProductResponse>> getProductsSorted(
            @RequestParam(value = "page", defaultValue = "1") int pageNumber,
            @RequestParam(value = "size", defaultValue = "10") int pageSize,
            @RequestParam(value = "sort", defaultValue = "asc") String sortDirection) {

        Sort.Direction sortDir = Sort.Direction.fromString(sortDirection);
        if (sortDir == null) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }

        List<ProductEntity> productEntities = productService.getProductsSorted(pageNumber, pageSize, sortDir);
        List<ProductResponse> response = productEntities.stream()
                .map(productMapper::productEntityToResponse)
                .collect(Collectors.toList());

        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @PostMapping
    @PreAuthorize("hasAnyAuthority('ADMIN')")
    @ResponseStatus(HttpStatus.CREATED)
    public ResponseEntity<ProductResponse> createProduct(@RequestBody ProductRequest productRequest) {
        ProductEntity productEntity = productMapper.productRequestToEntity(productRequest);
        ProductEntity createdProductEntity = productService.createProduct(productEntity);
        ProductResponse productResponse = productMapper.productEntityToResponse(createdProductEntity);
        return new ResponseEntity<>(productResponse, HttpStatus.CREATED);
    }

    @PutMapping(value = "/{id}")
    @PreAuthorize("hasAnyAuthority('ADMIN')")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<ProductResponse> updateProduct(@PathVariable("id") Long id, @RequestBody ProductRequest productRequest) {
        ProductEntity productEntity = productMapper.productRequestToEntity(productRequest);
        ProductEntity updatedProductEntity = productService.updateProduct(id, productEntity);
        ProductResponse productResponse = productMapper.productEntityToResponse(updatedProductEntity);
        return new ResponseEntity<>(productResponse, HttpStatus.OK);
    }

    @DeleteMapping("/{id}")
    @PreAuthorize("hasAnyAuthority('ADMIN')")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void deleteCategory(@PathVariable("id") Long id) {
        productService.deleteProduct(id);
    }
}
