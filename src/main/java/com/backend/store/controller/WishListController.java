package com.backend.store.controller;

import com.backend.store.dto.response.WishListResponse;
import com.backend.store.mapper.WishListMapper;
import com.backend.store.service.WishListService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import static org.springframework.http.MediaType.APPLICATION_JSON_VALUE;

@RestController
@RequestMapping(value = "/api/wishlist", produces = APPLICATION_JSON_VALUE,consumes = APPLICATION_JSON_VALUE)
public class WishListController {
    private final WishListService wishListService;
    private final WishListMapper wishListMapper;

    @Autowired
    public WishListController(WishListService wishListService, WishListMapper wishListMapper) {
        this.wishListService = wishListService;
        this.wishListMapper = wishListMapper;
    }

    @GetMapping("/{id}")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<WishListResponse> getWishListById(@PathVariable("id") Long id) {
        WishListResponse response = wishListMapper.wishListEntityToResponse(wishListService.getWishListById(id));
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @GetMapping("/user/{id}")
    @ResponseStatus(HttpStatus.OK)
    public ResponseEntity<WishListResponse> getWishListByUserId(@PathVariable("id") Long id) {
        WishListResponse response = wishListMapper.wishListEntityToResponse(wishListService.getWihListByUserId(id));
        return new ResponseEntity<>(response, HttpStatus.OK);
    }

    @PostMapping("/addProduct")
    public ResponseEntity<WishListResponse> addProductToWishList(
            @RequestParam("productId") Long productId,
            @RequestParam("userId") Long userId) {
        try {
            WishListResponse updatedWishList = wishListMapper.wishListEntityToResponse(wishListService.addProductToTheWishList(productId, userId));
            return new ResponseEntity<>(updatedWishList, HttpStatus.OK);
        } catch (IllegalArgumentException e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }

    @PostMapping("/deleteProduct")
    public ResponseEntity<WishListResponse> deleteProductFromWishList(
            @RequestParam("productId") Long productId,
            @RequestParam("userId") Long userId) {
        try {
            WishListResponse updatedWishList = wishListMapper.wishListEntityToResponse(wishListService.deleteProductFromWishList(productId,userId));
            return new ResponseEntity<>(updatedWishList, HttpStatus.OK);
        } catch (IllegalArgumentException e) {
            return new ResponseEntity<>(HttpStatus.BAD_REQUEST);
        }
    }


}
