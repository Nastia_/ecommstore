package com.backend.store.dto.request;

import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
public class AttributeValueRequest {
    String attributeValue;

    Long attributeId;
}
