package com.backend.store.dto.request;

import lombok.*;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
public class CategoryRequest {
    String title;
    private List<ProductRequest> products;
}
