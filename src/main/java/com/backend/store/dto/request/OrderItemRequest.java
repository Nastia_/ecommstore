package com.backend.store.dto.request;

import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
public class OrderItemRequest {
    private Long productId;
    private Long orderId;
    Integer quantity;

}
