package com.backend.store.dto.request;

import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.*;

import java.math.BigDecimal;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
public class ProductRequest {
    @NotBlank(message = "SKU must not be blank")
    @Size(min = 6, max = 12, message = "SKU must be between 6 and 12 characters long")
    private String sku;

    @NotBlank(message = "Title must not be blank")
    private String title;

    @NotBlank(message = "Description must not be blank")
    private String description;

    @NotNull(message = "Price must not be null")
    private BigDecimal price;

    @NotNull(message = "Category ID must not be null")
    private Long categoryId;

    private List<Long> attributeValueIds;
}
