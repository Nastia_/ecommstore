package com.backend.store.dto.request;

import com.backend.store.enums.UserRole;
import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
public class UserRequest {
    private String firstName;
    private String lastName;
    private String email;
    private UserRole userRole;
    private String password;

}
