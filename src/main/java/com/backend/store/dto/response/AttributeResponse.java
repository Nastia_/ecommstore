package com.backend.store.dto.response;

import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
public class AttributeResponse {
    private Long id;

    String attributeTitle;

}
