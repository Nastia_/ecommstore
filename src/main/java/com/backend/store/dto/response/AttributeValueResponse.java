package com.backend.store.dto.response;

import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
public class AttributeValueResponse {
    private Long id;
    String attributeValue;
    String attributeTitle;

}
