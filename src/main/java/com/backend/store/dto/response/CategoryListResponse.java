package com.backend.store.dto.response;

import lombok.*;

import java.time.Instant;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
public class CategoryListResponse {
    private Long id;
    private String title;
    private Instant createdAt;
    private Instant updatedAt;
}
