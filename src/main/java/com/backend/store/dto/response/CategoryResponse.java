package com.backend.store.dto.response;

import lombok.*;

import java.time.Instant;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
public class CategoryResponse {
    private Long id;
    private String title;
    private Instant createdAt;
    private Instant updatedAt;
    private List<ProductResponse> productList;
}
