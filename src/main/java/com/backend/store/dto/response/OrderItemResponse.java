package com.backend.store.dto.response;

import lombok.*;

import java.math.BigDecimal;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
public class OrderItemResponse {
    private Long id;
    private Long productId;
    private Long orderId;
    private BigDecimal pricePerProduct;
    private BigDecimal subTotal;
    private Integer quantity;


}
