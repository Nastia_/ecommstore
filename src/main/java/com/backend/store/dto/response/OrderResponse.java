package com.backend.store.dto.response;

import lombok.*;

import java.math.BigDecimal;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
public class OrderResponse {
    Long id;
    UsersOrderResponse user;
    List<OrderItemResponse> orderItems;
    BigDecimal totalPrice;

}
