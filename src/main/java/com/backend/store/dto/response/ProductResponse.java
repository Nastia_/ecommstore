package com.backend.store.dto.response;

import lombok.*;

import java.math.BigDecimal;
import java.time.Instant;
import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
public class ProductResponse {
    private Long id;
    private String sku;
    private String title;
    private String description;
    private BigDecimal price;
    private Instant createdAt;
    private Instant updatedAt;
    private List<AttributeValueResponse> attributeValueResponseList;

}
