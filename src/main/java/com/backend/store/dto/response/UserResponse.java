package com.backend.store.dto.response;

import com.backend.store.enums.UserRole;
import lombok.*;

import java.time.Instant;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
public class UserResponse {
    private Long id;
    private String firstName;
    private String lastName;
    private String email;
    private UserRole userRole;
    private Instant createdAt;
    private Instant updatedAt;


}