package com.backend.store.dto.response;

import lombok.*;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
public class UsersOrderResponse {
    private Long id;
    private String firstName;
    private String lastName;
    private String email;
}
