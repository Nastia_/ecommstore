package com.backend.store.dto.response;

import lombok.*;

import java.util.List;

@AllArgsConstructor
@NoArgsConstructor
@Builder
@Getter
@Setter
public class WishListResponse {
    Long wishListId;
    Long userId;
    List<ProductResponse> productResponseList;
}
