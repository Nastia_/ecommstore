package com.backend.store.entity;

import jakarta.persistence.*;
import jakarta.validation.constraints.NotBlank;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Entity
@Table(name = "attribute_value")
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class AttributeValueEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;

    @Column(name = "attribute_value")
    @NotBlank(message = "This field shouldn't be empty or contain only whitespaces")
    String attributeValue;

    @ManyToOne(fetch = FetchType.LAZY, cascade = CascadeType.MERGE)
    @JoinColumn(name = "attribute_id", nullable = false)
    AttributeEntity attribute;


}
