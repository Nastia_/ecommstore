package com.backend.store.entity;

import jakarta.persistence.*;
import jakarta.validation.constraints.DecimalMin;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.NotNull;
import jakarta.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;

import java.time.Instant;
import java.math.BigDecimal;
import java.util.List;

@Entity
@Table(name = "product")
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class ProductEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;
    @Column(name = "sku")
    @NotBlank(message = "This field shouldn't be empty or contain only whitespaces")
    @Size(min = 6, max = 12, message = "SKU must be between 6 and 12 characters long")
    String sku;

    @Column(name = "title")
    @NotBlank(message = "This field shouldn't be empty or contain only whitespaces")
    String title;

    @Column(name = "description")
    @NotBlank(message = "This field shouldn't be empty or contain only whitespaces")
    String description;

    @Column(name = "price")
    @NotNull(message = "This field shouldn't be empty")
    @DecimalMin(value = "0.01", message = "Price must be greater than 0")
    BigDecimal price;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "category_id")
    CategoryEntity category;


    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name="product_attribute_value",
            joinColumns=@JoinColumn(name="product_id"),
            inverseJoinColumns=@JoinColumn(name="attribute_value_id"))
            List<AttributeValueEntity> attributeValues;

    @CreationTimestamp
    @Column(name = "created_at")
    Instant createdAt;

    @UpdateTimestamp
    @Column(name = "updated_at")
    Instant updatedAt;


}

