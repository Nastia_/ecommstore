package com.backend.store.entity;

import com.backend.store.enums.UserRole;
import com.fasterxml.jackson.annotation.JsonIgnore;
import jakarta.persistence.*;
import jakarta.validation.constraints.Email;
import jakarta.validation.constraints.NotBlank;
import jakarta.validation.constraints.Size;
import lombok.*;
import org.hibernate.annotations.CreationTimestamp;
import org.hibernate.annotations.UpdateTimestamp;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

import java.time.Instant;
import java.util.Collection;
import java.util.List;


@Table(name = "customer")
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
@Builder
@Entity
public class UserEntity implements UserDetails {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    Long id;
    @Column(name = "first_name")
    @NotBlank(message = "This field shouldn't be empty or contain only whitespaces")
    String firstName;

    @Column(name = "last_name")
    @NotBlank(message = "This field shouldn't be empty or contain only whitespaces")
    String lastName;

    @Column(name = "email", unique = true)
    @NotBlank(message = "This field shouldn't be empty or contain only whitespaces")
    @Email
    String email;

    @JsonIgnore
    @Column(name = "password")
    @NotBlank(message = "This field shouldn't be empty or contain only whitespaces")
    @Size(min = 6, message = "Password must be at least 6 characters long")
    String password;

    @Column(name = "user_role", nullable = false)
    @Enumerated(EnumType.STRING)
    @JsonIgnore
    UserRole userRole;

    @OneToOne(mappedBy = "user", cascade = CascadeType.ALL)
    WishListEntity wishList;

    @CreationTimestamp
    @Column(name = "created_at")
    Instant createdAt;

    @UpdateTimestamp
    @Column(name = "updated_at")
    Instant updatedAt;

    @OneToMany(mappedBy = "user", cascade = CascadeType.ALL)
    List<Token> tokens;


    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return List.of(new SimpleGrantedAuthority(userRole.name()));
    }


    @Override
    public String getUsername() {
        return email;
    }

    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    @Override
    public boolean isEnabled() {
        return true;
    }
}
