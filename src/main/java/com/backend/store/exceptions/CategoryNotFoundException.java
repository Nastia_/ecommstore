package com.backend.store.exceptions;

import jakarta.validation.constraints.NotNull;

public class CategoryNotFoundException extends Exception {
    public CategoryNotFoundException(@NotNull(message = "Category ID must not be null") Long categoryId) {
    }
}
