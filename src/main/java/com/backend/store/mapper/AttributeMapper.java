package com.backend.store.mapper;

import com.backend.store.dto.request.AttributeRequest;
import com.backend.store.dto.response.AttributeResponse;
import com.backend.store.entity.AttributeEntity;
import org.springframework.stereotype.Component;

@Component
public class AttributeMapper {
    public AttributeEntity AttributeRequestToEntity(AttributeRequest attributeRequest) {
        AttributeEntity attribute = new AttributeEntity();
        attribute.setAttributeTitle(attributeRequest.getAttributeTitle());

        return attribute;
    }

    public AttributeResponse AttributeEntityToResponse(AttributeEntity attribute) {
        AttributeResponse attributeResponse = new AttributeResponse();
        attributeResponse.setId(attribute.getId());
        attributeResponse.setAttributeTitle(attribute.getAttributeTitle());

        return attributeResponse;
    }
}
