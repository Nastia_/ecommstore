package com.backend.store.mapper;

import com.backend.store.dto.request.AttributeValueRequest;
import com.backend.store.dto.response.AttributeValueResponse;
import com.backend.store.entity.AttributeEntity;
import com.backend.store.entity.AttributeValueEntity;
import com.backend.store.repository.AttributeRepository;
import com.backend.store.service.AttributeService;
import org.springframework.stereotype.Component;

@Component
public class AttributeValueMapper {
    private final AttributeRepository attributeRepository;
    private final AttributeService attributeService;

    public AttributeValueMapper(AttributeRepository attributeRepository, AttributeService attributeService) {
        this.attributeRepository = attributeRepository;
        this.attributeService = attributeService;
    }

    public AttributeValueEntity AttributeValueRequestToEntity(AttributeValueRequest attributeValueRequest) {
        AttributeValueEntity attributeValue = new AttributeValueEntity();
        attributeValue.setAttributeValue(attributeValueRequest.getAttributeValue());
        AttributeEntity attribute = attributeService.getAttributeById(attributeValueRequest.getAttributeId());

        attributeValue.setAttribute(attribute);

        return attributeValue;
    }

    public AttributeValueResponse AttributeValueEntityToResponse(AttributeValueEntity attributeValue) {
        AttributeValueResponse attributeValueResponse = new AttributeValueResponse();
        attributeValueResponse.setId(attributeValue.getId());
        attributeValueResponse.setAttributeTitle(attributeValue.getAttribute().getAttributeTitle());
        attributeValueResponse.setAttributeValue(attributeValue.getAttributeValue());

        return attributeValueResponse;
    }
}
