package com.backend.store.mapper;

import com.backend.store.dto.request.CategoryRequest;
import com.backend.store.dto.request.ProductRequest;
import com.backend.store.dto.response.CategoryListResponse;
import com.backend.store.dto.response.CategoryResponse;
import com.backend.store.dto.response.ProductResponse;
import com.backend.store.entity.CategoryEntity;
import com.backend.store.entity.ProductEntity;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class CategoryMapper {
    private final ProductMapper productMapper;

    @Autowired
    public CategoryMapper(ProductMapper productMapper) {
        this.productMapper = productMapper;
    }

    public CategoryEntity categoryRequestToEntity(CategoryRequest categoryRequest) {
        CategoryEntity category = new CategoryEntity();
        category.setTitle(categoryRequest.getTitle());

        List<ProductEntity> productEntities = new ArrayList<>();
        for (ProductRequest productRequest : categoryRequest.getProducts()) {
            ProductEntity product = productMapper.productRequestToEntity(productRequest);
            productEntities.add(product);
        }

        category.setProductList(productEntities);
        return category;
    }

    public CategoryResponse categoryEntityToResponse(CategoryEntity category) {
        CategoryResponse categoryResponse = new CategoryResponse();
        categoryResponse.setId(category.getId());
        categoryResponse.setTitle(category.getTitle());

        List<ProductResponse> productResponseList;
        if (!category.getProductList().isEmpty()) {
            productResponseList = category.getProductList().stream()
                    .map(productMapper::productEntityToResponse)
                    .collect(Collectors.toList());
        } else {
            productResponseList = Collections.emptyList();
        }

        categoryResponse.setProductList(productResponseList);
        categoryResponse.setCreatedAt(category.getCreatedAt());
        categoryResponse.setUpdatedAt(category.getUpdatedAt());
        return categoryResponse;
    }

    public CategoryListResponse categoryEntityToResponseList(CategoryEntity category) {
        CategoryListResponse categoryResponse = new CategoryListResponse();
        categoryResponse.setId(category.getId());
        categoryResponse.setTitle(category.getTitle());
        categoryResponse.setCreatedAt(category.getCreatedAt());
        categoryResponse.setUpdatedAt(category.getUpdatedAt());
        return categoryResponse;
    }
}
