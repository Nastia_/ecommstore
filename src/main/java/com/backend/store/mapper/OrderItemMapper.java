package com.backend.store.mapper;

import com.backend.store.dto.request.OrderItemRequest;
import com.backend.store.dto.response.OrderItemResponse;
import com.backend.store.entity.OrderItemEntity;
import com.backend.store.service.OrderService;
import com.backend.store.service.ProductService;
import org.springframework.stereotype.Component;

@Component
public class OrderItemMapper {
    private final OrderService orderService;
    private final ProductService productService;

    public OrderItemMapper(OrderService orderService, ProductService productService) {
        this.orderService = orderService;
        this.productService = productService;
    }

    public OrderItemEntity orderItemRequestToEntity(OrderItemRequest orderItemRequest){
        OrderItemEntity orderItem = new OrderItemEntity();
        orderItem.setOrder(orderService.getOrderById(orderItemRequest.getOrderId()));
        orderItem.setProduct(productService.getProductById(orderItemRequest.getProductId()));
//        orderItem.setOrderItemPrice(productService.getProductById(orderItemRequest.getProductId()).getPrice());
        orderItem.setQuantity(orderItemRequest.getQuantity());

        return orderItem;
    }
    public OrderItemResponse orderItemEntityToResponse(OrderItemEntity orderItemEntity){
        OrderItemResponse orderItemResponse = new OrderItemResponse();
        orderItemResponse.setId(orderItemEntity.getId());
        orderItemResponse.setOrderId(orderItemEntity.getOrder().getId());
        orderItemResponse.setPricePerProduct(orderItemEntity.getProduct().getPrice());
        orderItemResponse.setSubTotal(orderItemEntity.getSubTotalPrice());
        orderItemResponse.setQuantity(orderItemEntity.getQuantity());
        orderItemResponse.setProductId(orderItemEntity.getProduct().getId());

        return orderItemResponse;
    }

}
