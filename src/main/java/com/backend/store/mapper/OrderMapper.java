package com.backend.store.mapper;

import com.backend.store.dto.request.OrderRequest;
import com.backend.store.dto.response.OrderItemResponse;
import com.backend.store.dto.response.OrderResponse;
import com.backend.store.entity.OrderEntity;
import com.backend.store.entity.OrderItemEntity;
import com.backend.store.service.*;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class OrderMapper {
    private final UserService userService;
    private final UserMapper userMapper;


    private final OrderItemMapper orderItemMapper;

    public OrderMapper(UserService userService, UserMapper userMapper, OrderItemMapper orderItemMapper) {
        this.userService = userService;
        this.userMapper = userMapper;
        this.orderItemMapper = orderItemMapper;
    }

    public OrderEntity orderRequestToEntity(OrderRequest orderRequest) {
        OrderEntity orderEntity = new OrderEntity();
        orderEntity.setUser(userService.getUserById(orderRequest.getUserId()));
        return orderEntity;
    }

    public OrderResponse orderEntityToResponse(OrderEntity orderEntity) {
        OrderResponse orderResponse = new OrderResponse();
        orderResponse.setId(orderEntity.getId());
        orderResponse.setUser(userMapper.userEntityUsersOrderResponse(orderEntity.getUser()));

        List<OrderItemResponse> orderItemResponses = new ArrayList<>();
        List<OrderItemEntity> orderItemEntities = orderEntity.getOrderItems();
        if (orderItemEntities != null) {
            for (OrderItemEntity orderItem : orderItemEntities) {
                orderItemResponses.add(orderItemMapper.orderItemEntityToResponse(orderItem));
            }
        }
        orderResponse.setOrderItems(orderItemResponses);
        orderResponse.setTotalPrice(orderEntity.getTotalPrice());

        return orderResponse;
    }
}
