package com.backend.store.mapper;

import com.backend.store.dto.request.ProductRequest;
import com.backend.store.dto.response.AttributeValueResponse;
import com.backend.store.dto.response.ProductResponse;
import com.backend.store.entity.AttributeValueEntity;
import com.backend.store.entity.CategoryEntity;
import com.backend.store.entity.ProductEntity;
import com.backend.store.service.AttributeValueService;
import com.backend.store.service.CategoryService;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class ProductMapper {
    private final CategoryService categoryService;
    private final AttributeValueService attributeValueService;
    private final AttributeValueMapper attributeValueMapper;

    public ProductMapper(CategoryService categoryService, AttributeValueService attributeValueService, AttributeValueMapper attributeValueMapper) {
        this.categoryService = categoryService;
        this.attributeValueService = attributeValueService;
        this.attributeValueMapper = attributeValueMapper;
    }

    public ProductEntity productRequestToEntity(ProductRequest productRequest) {
        ProductEntity product = new ProductEntity();
        product.setSku(productRequest.getSku());
        product.setTitle(productRequest.getTitle());
        product.setDescription(productRequest.getDescription());
        product.setPrice(productRequest.getPrice());

        CategoryEntity categoryEntity = categoryService.getCategoryById(productRequest.getCategoryId());
        product.setCategory(categoryEntity);

        List<AttributeValueEntity> attributeValueEntityList = new ArrayList<>();

        for (Long attributeValueId : productRequest.getAttributeValueIds()) {
            AttributeValueEntity attributeValue = attributeValueService.getAttributeValueById(attributeValueId);
            attributeValueEntityList.add(attributeValue);
        }
        product.setAttributeValues(attributeValueEntityList);

        return product;
    }

    public ProductResponse productEntityToResponse(ProductEntity product) {
        ProductResponse productResponse = new ProductResponse();
        productResponse.setId(product.getId());
        productResponse.setSku(product.getSku());
        productResponse.setTitle(product.getTitle());
        productResponse.setDescription(product.getDescription());
        productResponse.setPrice(product.getPrice());
        productResponse.setCreatedAt(product.getCreatedAt());
        productResponse.setUpdatedAt(product.getUpdatedAt());


        List<AttributeValueResponse> attributeValueResponseList = new ArrayList<>();
        List<AttributeValueEntity> attributeValueEntityList1 = product.getAttributeValues();
        for(AttributeValueEntity attributeValue: attributeValueEntityList1){
            attributeValueResponseList.add(attributeValueMapper.AttributeValueEntityToResponse(attributeValue));
        }
        productResponse.setAttributeValueResponseList(attributeValueResponseList);

        return productResponse;

    }
}

