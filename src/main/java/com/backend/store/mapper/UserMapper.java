package com.backend.store.mapper;

import com.backend.store.dto.request.UserRequest;
import com.backend.store.dto.response.UserResponse;
import com.backend.store.dto.response.UsersOrderResponse;
import com.backend.store.entity.UserEntity;
import com.backend.store.service.WishListService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class UserMapper {

    public UserEntity userRequestToEntity(UserRequest userRequest) {
        UserEntity user = new UserEntity();
        user.setFirstName(userRequest.getFirstName());
        user.setLastName(userRequest.getLastName());
        user.setEmail(userRequest.getEmail());
        user.setUserRole(userRequest.getUserRole());
        user.setPassword(userRequest.getPassword());
        return user;
    }

    public UserResponse userEntityToResponse(UserEntity userEntity) {
        UserResponse userResponse = new UserResponse();
        userResponse.setId(userEntity.getId());
        userResponse.setFirstName(userEntity.getFirstName());
        userResponse.setLastName(userEntity.getLastName());
        userResponse.setEmail(userEntity.getEmail());
        userResponse.setUserRole(userEntity.getUserRole());
        userResponse.setCreatedAt(userEntity.getCreatedAt());
        userResponse.setUpdatedAt(userEntity.getUpdatedAt());
        return userResponse;
    }
    public UsersOrderResponse  userEntityUsersOrderResponse(UserEntity userEntity) {
        UsersOrderResponse usersOrderResponse = new UsersOrderResponse();
        usersOrderResponse.setId(userEntity.getId());
        usersOrderResponse.setEmail(userEntity.getEmail());
        usersOrderResponse.setFirstName(userEntity.getFirstName());
        usersOrderResponse.setLastName(userEntity.getLastName());
        return usersOrderResponse;
    }
}
