package com.backend.store.mapper;

import com.backend.store.dto.request.WishListRequest;
import com.backend.store.dto.response.ProductResponse;
import com.backend.store.dto.response.WishListResponse;
import com.backend.store.entity.ProductEntity;
import com.backend.store.entity.WishListEntity;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;

@Component
public class WishListMapper {
    private final ProductMapper productMapper;

    public WishListMapper(ProductMapper productMapper) {
        this.productMapper = productMapper;
    }

    public WishListEntity wishListRequestToEntity(WishListRequest wishListRequest) {

        return null;
    }
    public WishListResponse wishListEntityToResponse(WishListEntity wishListEntity) {

        WishListResponse wishListResponse = new WishListResponse();
        wishListResponse.setWishListId(wishListEntity.getId());
        wishListResponse.setUserId(wishListEntity.getUser().getId());
        List<ProductEntity> productEntities = wishListEntity.getProducts();
        List<ProductResponse> productResponseList = new ArrayList<>();
        for (ProductEntity productEntity: productEntities){
            productResponseList.add(productMapper.productEntityToResponse(productEntity));
        }
        wishListResponse.setProductResponseList(productResponseList);

        return wishListResponse;
    }
}
