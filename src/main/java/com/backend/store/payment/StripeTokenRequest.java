package com.backend.store.payment;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class StripeTokenRequest {
    private String cardNumber;
    private String exMonth;
    private String expYear;
    private String cvc;
    private String token;
    private String username;
    private boolean success;

}
