package com.backend.store.repository;

import com.backend.store.entity.AttributeValueEntity;
import com.backend.store.entity.ProductEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface AttributeValueRepository extends JpaRepository<AttributeValueEntity,Long> {
    List<AttributeValueEntity> findAllByAttributeValueIn(List<String> attributeValues);
}
