package com.backend.store.repository;

import com.backend.store.entity.CategoryEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface CategoryRepository extends JpaRepository<CategoryEntity, Long> {
    public CategoryEntity findByTitle(String title);
}
