package com.backend.store.repository;

import com.backend.store.entity.WishListEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface WishListRepository extends JpaRepository<WishListEntity, Long> {
    WishListEntity findByUserId(Long id);
}
