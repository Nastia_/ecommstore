package com.backend.store.service;

import com.backend.store.entity.AttributeEntity;

import java.util.List;

public interface AttributeService {
    AttributeEntity getAttributeById(Long id);

    List<AttributeEntity> getAllAttributes();

    AttributeEntity createAttribute(AttributeEntity attributeEntity);

    AttributeEntity updateAttribute(Long id, AttributeEntity attributeEntity);

    void deleteAttribute(Long id);

}
