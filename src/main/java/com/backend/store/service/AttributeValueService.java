package com.backend.store.service;

import com.backend.store.entity.AttributeValueEntity;

import java.util.List;

public interface AttributeValueService {
    AttributeValueEntity getAttributeValueById(Long id);

    List<AttributeValueEntity> getAllAttributeValues();

    AttributeValueEntity  createAttributeValue(AttributeValueEntity  attributeValueEntity);

    AttributeValueEntity  updateAttributeValue(Long id, AttributeValueEntity  attributeValueEntity);

    void deleteAttributeValue(Long id);
}
