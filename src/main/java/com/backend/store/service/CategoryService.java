package com.backend.store.service;

import com.backend.store.entity.CategoryEntity;
import org.springframework.data.domain.Pageable;


import java.util.List;

public interface CategoryService {
    CategoryEntity getCategoryById(Long id);

    List<CategoryEntity> getAllCategories();

    CategoryEntity createCategory(CategoryEntity categoryEntity);

    CategoryEntity updateCategory(Long id, CategoryEntity categoryEntity);

    void deleteCategory(Long id);


}
