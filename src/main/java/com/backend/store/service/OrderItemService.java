package com.backend.store.service;

import com.backend.store.entity.OrderEntity;
import com.backend.store.entity.OrderItemEntity;

import java.math.BigDecimal;
import java.util.List;

public interface OrderItemService {
    OrderItemEntity getOrderItemEntityById(Long id);

    List<OrderItemEntity> getAllOrderItems();

    OrderItemEntity createOrderItem(OrderItemEntity orderItemEntity);

    OrderItemEntity updateOrderItem(Long id, OrderItemEntity orderItemEntity);

    void deleteOrderItem(Long id);


}
