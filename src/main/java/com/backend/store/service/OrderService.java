package com.backend.store.service;

import com.backend.store.entity.OrderEntity;

import java.math.BigDecimal;
import java.util.List;

public interface OrderService {
    OrderEntity getOrderById(Long id);

    List<OrderEntity> getAllOrders();

    OrderEntity createOrder(OrderEntity orderEntity);

    OrderEntity updateOrder(Long id, OrderEntity orderEntity);

    void deleteOrder(Long id);

    OrderEntity updateTotalPrice(Long id, BigDecimal newTotalPrice, boolean isAddition);

    String processPayment(Long orderId, String token);

}
