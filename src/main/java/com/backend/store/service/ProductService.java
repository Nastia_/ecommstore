package com.backend.store.service;

import com.backend.store.dto.response.ProductResponse;
import com.backend.store.entity.ProductEntity;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Sort;

import java.util.List;

public interface ProductService {

    ProductEntity getProductById(Long id);

    List<ProductEntity> getAllProducts();

    ProductEntity createProduct(ProductEntity productEntity);

    ProductEntity updateProduct(Long id, ProductEntity productEntity);

    void deleteProduct(Long id);

    List<ProductEntity> getProductsByPagination(int pageNo, int pageSize);

    List<ProductEntity> getProductsByCategoryAndPagination(int pageNo, int pageSize, Long categoryId);

    List<ProductEntity> getProductsByPartialTitle(int pageNo, int pageSize, String partialTitle);

    List<ProductEntity> getProductsByPartialTitleAndSort(int pageNo, int pageSize, String partialTitle, Sort.Direction sortDirection);

    List<ProductEntity> getProductsSorted(int pageNo, int pageSize, Sort.Direction sortDirection);

    ProductResponse addCategoryToProduct(Long productId, Long categoryId);
}
