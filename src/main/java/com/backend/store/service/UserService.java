package com.backend.store.service;

import com.backend.store.entity.UserEntity;

import java.util.List;

public interface UserService {
    UserEntity getUserById(Long id);

    List<UserEntity> getAllUsers();

    UserEntity createUser(UserEntity userEntity);

    UserEntity updateUser(Long id, UserEntity userEntity);

    void deleteUser(Long id);


}
