package com.backend.store.service;

import com.backend.store.entity.UserEntity;
import com.backend.store.entity.WishListEntity;

import java.util.List;

public interface WishListService {

    WishListEntity getWishListById(Long id);

    WishListEntity getWihListByUserId(Long id);

    List<WishListEntity> getAllWishLists();

    WishListEntity createWishList(WishListEntity wishListEntity);

    WishListEntity updateWishList(Long id, WishListEntity wishListEntity);

    WishListEntity addProductToTheWishList(Long productId, Long userId);

    void deleteWishListEntity(Long id);

    WishListEntity deleteProductFromWishList(Long productId, Long userid);


}
