package com.backend.store.service.impl;

import com.backend.store.dto.request.AttributeRequest;
import com.backend.store.dto.response.AttributeResponse;
import com.backend.store.entity.AttributeEntity;
import com.backend.store.mapper.AttributeMapper;
import com.backend.store.repository.AttributeRepository;
import com.backend.store.repository.AttributeValueRepository;
import com.backend.store.service.AttributeService;
import org.hibernate.ObjectNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class AttributeServiceImpl implements AttributeService {
    AttributeRepository attributeRepository;
    AttributeValueRepository attributeValueRepository;
    AttributeMapper attributeMapper;

    @Autowired
    public AttributeServiceImpl(AttributeRepository attributeRepository, AttributeValueRepository attributeValueRepository, AttributeMapper attributeMapper) {
        this.attributeRepository = attributeRepository;
        this.attributeValueRepository = attributeValueRepository;
        this.attributeMapper = attributeMapper;
    }

    @Override
    public AttributeEntity getAttributeById(Long id) {
        if (id == null) {
            throw new IllegalArgumentException("Attribute ID cannot be null");
        }
        AttributeEntity attributeEntity = attributeRepository.findById(id)
                .orElseThrow(() -> new ObjectNotFoundException("Attribute was not found with ID: ", id));

        return attributeEntity;
    }

    @Override
    public List<AttributeEntity> getAllAttributes() {
        List<AttributeEntity> attributeEntities = attributeRepository.findAll();
        return attributeEntities;
    }

    @Override
    public AttributeEntity createAttribute(AttributeEntity attributeEntity) {
        if (attributeEntity == null) {
            throw new IllegalArgumentException("Attribute cannot be null");
        }
        AttributeEntity savedAttribute = attributeRepository.save(attributeEntity);
        return savedAttribute;

    }

    @Override
    public  AttributeEntity updateAttribute(Long id,  AttributeEntity attributeEntity) {
        if (id == null) {
            throw new IllegalArgumentException("Attribute ID cannot be null");
        }

        AttributeEntity attribute = attributeRepository.findById(id)
                .orElseThrow(() -> new ObjectNotFoundException("Attribute was not found with ID: ", id));

        attribute.setAttributeTitle(attributeEntity.getAttributeTitle());

        AttributeEntity savedAttribute = attributeRepository.save(attribute);

        return savedAttribute;

    }

    @Override
    public void deleteAttribute(Long id) {
        if (id == null) {
            throw new IllegalArgumentException("Attribute ID cannot be null");
        }
        try {
            attributeRepository.deleteById(id);
        } catch (EmptyResultDataAccessException e) {
            throw new ObjectNotFoundException("Attribute with ID " + id + " not found", e);
        }
    }
}
