package com.backend.store.service.impl;

import com.backend.store.entity.AttributeEntity;
import com.backend.store.entity.AttributeValueEntity;
import com.backend.store.mapper.AttributeValueMapper;
import com.backend.store.repository.AttributeRepository;
import com.backend.store.repository.AttributeValueRepository;
import com.backend.store.repository.ProductRepository;
import com.backend.store.service.AttributeValueService;
import org.hibernate.ObjectNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AttributeValueServiceImpl implements AttributeValueService {
    AttributeValueRepository attributeValueRepository;
    AttributeRepository attributeRepository;
    ProductRepository productRepository;

    @Autowired
    public AttributeValueServiceImpl(AttributeValueRepository attributeValueRepository, AttributeRepository attributeRepository, ProductRepository productRepository) {
        this.attributeValueRepository = attributeValueRepository;
        this.attributeRepository = attributeRepository;
        this.productRepository = productRepository;
    }

    @Override
    public AttributeValueEntity getAttributeValueById(Long id) {
        if (id == null) {
            throw new IllegalArgumentException("Attribute value ID cannot be null");
        }
        AttributeValueEntity attributeValueEntity = attributeValueRepository.findById(id)
                .orElseThrow(() -> new ObjectNotFoundException("Attribute value was not found with ID: ", id));

        return attributeValueEntity;
    }

    @Override
    public List<AttributeValueEntity> getAllAttributeValues() {
        List<AttributeValueEntity> attributeValueEntities = attributeValueRepository.findAll();
        return attributeValueEntities;
    }

    @Override
    public AttributeValueEntity createAttributeValue(AttributeValueEntity attributeValueEntity) {
        if (attributeValueEntity == null) {
            throw new IllegalArgumentException("Attribute value cannot be null");
        }
        AttributeValueEntity savedAttributeValue = attributeValueRepository.save(attributeValueEntity);

        return savedAttributeValue;
    }

    @Override
    public AttributeValueEntity  updateAttributeValue(Long id, AttributeValueEntity attributeValueEntity) {
        if (id == null) {
            throw new IllegalArgumentException("Attribute value ID cannot be null");
        }

        AttributeValueEntity attributeValue = attributeValueRepository.findById(id)
                .orElseThrow(() -> new ObjectNotFoundException("Attribute Value was not found with ID: ", id));

        AttributeEntity attributeEntity = attributeRepository.findById(attributeValueEntity.getAttribute().getId()).get();
        attributeValue.setAttributeValue(attributeValueEntity.getAttributeValue());
        attributeValue.setAttribute(attributeEntity);

        AttributeValueEntity savedAttributeValue = attributeValueRepository.save(attributeValue);

        return savedAttributeValue;
    }

    @Override
    public void deleteAttributeValue(Long id) {
        if (id == null) {
            throw new IllegalArgumentException("Attribute value ID cannot be null");
        }
        try {
            attributeValueRepository.deleteById(id);
        } catch (EmptyResultDataAccessException e) {
            throw new ObjectNotFoundException("Attribute value with ID " + id + " not found", e);
        }

    }
}
