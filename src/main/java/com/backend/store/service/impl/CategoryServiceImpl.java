package com.backend.store.service.impl;

import com.backend.store.entity.CategoryEntity;
import com.backend.store.entity.ProductEntity;
import com.backend.store.repository.CategoryRepository;
import com.backend.store.repository.ProductRepository;
import com.backend.store.service.CategoryService;
import org.hibernate.ObjectNotFoundException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;
;
import java.util.List;

@Service
public class CategoryServiceImpl implements CategoryService {
    CategoryRepository categoryRepository;
    ProductRepository productRepository;

    public CategoryServiceImpl(CategoryRepository categoryRepository, ProductRepository productRepository) {
        this.categoryRepository = categoryRepository;
        this.productRepository = productRepository;
    }

    @Override
    public CategoryEntity getCategoryById(Long id) {

        if (id == null) {
            throw new IllegalArgumentException("Category ID cannot be null");
        }
        CategoryEntity categoryEntity = categoryRepository.findById(id)
                .orElseThrow(() -> new ObjectNotFoundException("Category was not found with ID: ", id));
        return categoryEntity;
    }

    @Override
    public List<CategoryEntity> getAllCategories() {
        List<CategoryEntity> categories = categoryRepository.findAll();
        return categories;
    }

    private CategoryEntity createDefaultCategory() {
        CategoryEntity existingDefaultCategory = categoryRepository.findByTitle("Default Category");

        if (existingDefaultCategory != null) {
            return existingDefaultCategory;
        }

        CategoryEntity newDefaultCategory = new CategoryEntity();
        newDefaultCategory.setTitle("Default Category");

        return categoryRepository.save(newDefaultCategory);
    }

    @Override
    public CategoryEntity createCategory(CategoryEntity categoryEntity) {
        if (categoryEntity == null) {
            throw new IllegalArgumentException("Category cannot be null");
        }

        if (isDefaultCategory(categoryEntity)) {
            return createDefaultCategory();
        }

        return categoryRepository.save(categoryEntity);
    }

    private boolean isDefaultCategory(CategoryEntity categoryEntity) {
        return "Default Category".equals(categoryEntity.getTitle());
    }


    @Override
    public CategoryEntity updateCategory(Long id, CategoryEntity categoryEntity) {
        if (categoryEntity == null) {
            throw new IllegalArgumentException("CategoryRequest cannot be null");
        }
        CategoryEntity category = categoryRepository.findById(id)
                .orElseThrow(() -> new RuntimeException("Category with ID " + id + " not found"));

        category.setTitle(categoryEntity.getTitle());
        List<ProductEntity> productEntities = categoryEntity.getProductList();
        category.setProductList(productEntities);

        CategoryEntity updatedCategory = categoryRepository.save(category);

        return updatedCategory;
    }

    @Override
    public void deleteCategory(Long id) {
        if (id == null) {
            throw new IllegalArgumentException("Category ID cannot be null");
        }

        CategoryEntity deletedCategory = categoryRepository.findById(id)
                .orElseThrow(() -> new ObjectNotFoundException("Category with ID not found ", id));

        CategoryEntity defaultCategory = createDefaultCategory();

        for (ProductEntity product : deletedCategory.getProductList()) {
            product.setCategory(defaultCategory);
        }

        productRepository.saveAll(deletedCategory.getProductList());

        try {
            categoryRepository.deleteById(id);
        } catch (EmptyResultDataAccessException e) {
            throw new ObjectNotFoundException("Category with ID " + id + " not found", e);
        }
    }





}
