package com.backend.store.service.impl;

import com.backend.store.entity.OrderEntity;
import com.backend.store.entity.OrderItemEntity;
import com.backend.store.repository.OrderItemRepository;
import com.backend.store.service.OrderItemService;
import com.backend.store.service.OrderService;
import org.hibernate.ObjectNotFoundException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.List;
import java.util.Objects;

@Service
public class OrderItemServiceImpl implements OrderItemService {
    OrderItemRepository orderItemRepository;
    OrderService orderService;


    public OrderItemServiceImpl(OrderItemRepository orderItemRepository, OrderService orderService) {
        this.orderItemRepository = orderItemRepository;
        this.orderService = orderService;
    }

    @Override
    public OrderItemEntity getOrderItemEntityById(Long id) {
        if (id == null) {
            throw new IllegalArgumentException("Attribute value ID cannot be null");
        }
        OrderItemEntity orderItemEntity = orderItemRepository.findById(id)
                .orElseThrow(() -> new ObjectNotFoundException("OrderItem value was not found with ID: ", id));

        return orderItemEntity;
    }

    @Override
    public List<OrderItemEntity> getAllOrderItems() {
        List<OrderItemEntity> orderItemEntities = orderItemRepository.findAll();

        return orderItemEntities;
    }

    @Override
    public OrderItemEntity createOrderItem(OrderItemEntity orderItemEntity) {
        if (orderItemEntity == null) {
            throw new IllegalArgumentException("OrderItem value cannot be null");
        }

        OrderEntity orderEntity = orderService.getOrderById(orderItemEntity.getOrder().getId());

        for (OrderItemEntity item : orderEntity.getOrderItems()) {
            if (Objects.equals(item.getProduct().getId(), orderItemEntity.getProduct().getId())) {

                int newQuantity = item.getQuantity() + orderItemEntity.getQuantity();
                int quantityDifference = newQuantity - item.getQuantity();
                item.setQuantity(newQuantity);
                item.setSubTotalPrice(item.getProduct().getPrice().multiply(BigDecimal.valueOf(newQuantity)));

                BigDecimal additionalTotalPrice = item.getProduct().getPrice().multiply(BigDecimal.valueOf(quantityDifference));

                orderService.updateTotalPrice(orderEntity.getId(), additionalTotalPrice, true);

                return orderItemRepository.save(item);
            }
        }

        orderItemEntity.setSubTotalPrice(orderItemEntity.getProduct().getPrice().multiply(BigDecimal.valueOf(orderItemEntity.getQuantity())));
        orderService.updateTotalPrice(orderEntity.getId(), orderItemEntity.getSubTotalPrice(), true);
        orderItemEntity.setOrder(orderEntity);

        return orderItemRepository.save(orderItemEntity);
    }

    @Override
    public OrderItemEntity updateOrderItem(Long id, OrderItemEntity orderItemEntity) {
        if (id == null) {
            throw new IllegalArgumentException("OrderItem ID cannot be null");
        }

        OrderItemEntity orderItem = orderItemRepository.findById(id)
                .orElseThrow(() -> new ObjectNotFoundException("Order item was not found with ID: ", id));

        boolean isAddition = false;
        if (orderItemEntity.getQuantity() > orderItem.getQuantity()) {
            isAddition = true;
        }

        BigDecimal originalSubTotalPrice = orderItem.getSubTotalPrice();

        orderItem.setProduct(orderItemEntity.getProduct());
        orderItem.setOrder(orderItemEntity.getOrder());
        orderItem.setQuantity(orderItemEntity.getQuantity());
        orderItem.setSubTotalPrice(orderItemEntity.getProduct().getPrice().multiply(BigDecimal.valueOf(orderItemEntity.getQuantity())));

        BigDecimal subTotalPriceDifference = orderItem.getSubTotalPrice().subtract(originalSubTotalPrice);

        orderService.updateTotalPrice(orderItem.getOrder().getId(), subTotalPriceDifference, isAddition);

        return orderItemRepository.save(orderItem);
    }


    @Override
    public void deleteOrderItem(Long id) {
        if (id == null) {
            throw new IllegalArgumentException("OrderItem ID cannot be null");
        }
        try {
            OrderItemEntity orderItem = orderItemRepository.findById(id)
                    .orElseThrow(() -> new ObjectNotFoundException("Order item was not found with ID: ", id));

            orderService.updateTotalPrice(orderItem.getOrder().getId(), orderItem.getSubTotalPrice(), false);
            orderItemRepository.deleteById(id);

        } catch (EmptyResultDataAccessException e) {
            throw new ObjectNotFoundException("OrderItem  with ID " + id + " not found", e);
        }

    }
}
