package com.backend.store.service.impl;

import com.backend.store.entity.OrderEntity;
import com.backend.store.repository.OrderRepository;
import com.backend.store.service.OrderService;
import com.stripe.Stripe;
import com.stripe.exception.StripeException;
import com.stripe.model.Charge;
import com.stripe.param.ChargeCreateParams;
import lombok.AllArgsConstructor;
import org.hibernate.ObjectNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class OrderServiceImpl implements OrderService {
    OrderRepository orderRepository;

    @Autowired
    public OrderServiceImpl(OrderRepository orderRepository) {
        this.orderRepository = orderRepository;
    }


    @Override
    public OrderEntity getOrderById(Long id) {
        if (id == null) {
            throw new IllegalArgumentException("Order value ID cannot be null");
        }
        OrderEntity orderEntity = orderRepository.findById(id)
                .orElseThrow(() -> new ObjectNotFoundException("Order value was not found with ID: ", id));

        return orderEntity;
    }

    @Override
    public List<OrderEntity> getAllOrders() {
        List<OrderEntity> orderEntities = orderRepository.findAll();

        return orderEntities;
    }

    @Override
    public OrderEntity createOrder(OrderEntity orderEntity) {
        if (orderEntity == null) {
            throw new IllegalArgumentException("Order  cannot be null");
        }

        OrderEntity savedOrder = orderRepository.save(orderEntity);

        return savedOrder;
    }

    @Override
    public OrderEntity updateOrder(Long id, OrderEntity orderEntity) {
        if (id == null) {
            throw new IllegalArgumentException("Order ID cannot be null");
        }
        OrderEntity order = orderRepository.findById(id)
                .orElseThrow(() -> new ObjectNotFoundException("Order was not found with ID: ", id));
        order.setUser(orderEntity.getUser());
//        order.setOrderItems(orderEntity.getOrderItems());
        order.setTotalPrice(orderEntity.getTotalPrice());

        return orderRepository.save(order);
    }

    @Override
    public void deleteOrder(Long id) {
        if (id == null) {
            throw new IllegalArgumentException("Order ID cannot be null");
        }
        try {
            orderRepository.deleteById(id);
        } catch (EmptyResultDataAccessException e) {
            throw new ObjectNotFoundException("Order  with ID " + id + " not found", e);
        }
    }

    private OrderEntity findRelatedOrder(Long id) {
        OrderEntity order = orderRepository.findById(id)
                .orElseThrow(() -> new ObjectNotFoundException("Order was not found with ID: ", id));
        return order;
    }

    @Override
    public OrderEntity updateTotalPrice(Long id, BigDecimal price, boolean isAddition) {
        OrderEntity order = findRelatedOrder(id);

        BigDecimal existingTotalPrice = order.getTotalPrice();
        if (existingTotalPrice == null) {
            existingTotalPrice = BigDecimal.ZERO;
        }
        if (!isAddition){
            BigDecimal accumulatedTotalPrice = existingTotalPrice.subtract(price);
            order.setTotalPrice(accumulatedTotalPrice);
        }
        else {
            BigDecimal subtractedTotalPrice = existingTotalPrice.add(price);
            order.setTotalPrice(subtractedTotalPrice);
        }
        return orderRepository.save(order);

    }

    @Value("${stripe.secret.key}")
    private String secretKey;

    public Charge chargeCreditCard(String token, Long orderId) throws StripeException {
        Stripe.apiKey = secretKey;
        OrderEntity order = getOrderById(orderId);

        Map<String, Object> params = new HashMap<>();
        params.put("amount", (int)(order.getTotalPrice().multiply(new BigDecimal(100)).intValue()));
        params.put("currency", "USD");
        params.put("description", "Payment for order #" + order.getId());
        params.put("source", token);

        Charge charge = Charge.create(params);
        return charge;
    }

    public String processPayment(Long orderId, String token) {
        Stripe.apiKey = secretKey;
        OrderEntity order = getOrderById(orderId);

        try {
            Charge charge = Charge.create(
                    new ChargeCreateParams.Builder()
                            .setAmount((long) order.getTotalPrice().multiply(new BigDecimal(100)).intValue()) // Amount in cents
                            .setCurrency("usd")
                            .setDescription("Payment for Order #" + order.getId())
                            .setSource(token) // Token obtained from the frontend
                            .build()
            );

            // Handle the result, update order status, etc.
            if (charge.getPaid()) {
                // Update your order entity
                order.setStatusPayment(true);
                // Save changes to the database
                orderRepository.save(order);
                return "Payment successful";
            } else {
                // Payment failed
                // Handle accordingly, for example, throw an exception or log an error
                return "Payment failed: " + charge.getFailureCode() + " - " + charge.getFailureMessage();
            }
        } catch (StripeException e) {
            // Handle Stripe-specific exceptions
            // You might want to log the exception or throw a custom exception
            return "Payment failed: " + e.getMessage();
        }
    }


}
