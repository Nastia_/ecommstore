package com.backend.store.service.impl;

import com.backend.store.dto.response.ProductResponse;
import com.backend.store.entity.AttributeValueEntity;
import com.backend.store.entity.CategoryEntity;
import com.backend.store.entity.ProductEntity;
import com.backend.store.mapper.CategoryMapper;
import com.backend.store.mapper.ProductMapper;
import com.backend.store.repository.AttributeValueRepository;
import com.backend.store.repository.CategoryRepository;
import com.backend.store.repository.ProductRepository;
import com.backend.store.service.AttributeValueService;
import com.backend.store.service.ProductService;
import jakarta.persistence.EntityNotFoundException;
import org.hibernate.ObjectNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class ProductServiceImpl implements ProductService {

    ProductRepository productRepository;
    ProductMapper productMapper;
    CategoryMapper categoryMapper;
    CategoryServiceImpl categoryService;
    CategoryRepository categoryRepository;

    AttributeValueRepository attributeValueRepository;
    AttributeValueService attributeValueService;


    @Autowired
    public ProductServiceImpl(ProductRepository productRepository,
                              ProductMapper productMapper,
                              CategoryMapper categoryMapper,
                              CategoryServiceImpl categoryService,
                              CategoryRepository categoryRepository,
                              AttributeValueRepository attributeValueRepository,
                              AttributeValueService attributeValueService) {
        this.productRepository = productRepository;
        this.productMapper = productMapper;
        this.categoryMapper = categoryMapper;
        this.categoryService = categoryService;
        this.categoryRepository = categoryRepository;
        this.attributeValueRepository = attributeValueRepository;
        this.attributeValueService = attributeValueService;
    }

    @Override
    public ProductEntity getProductById(Long id) {
        if (id == null) {
            throw new IllegalArgumentException("Product ID cannot be null");
        }
        ProductEntity productEntity = productRepository.findById(id).orElseThrow(() -> new ObjectNotFoundException("Product was not found  ID: ", id));
        return productEntity;
    }


    @Override
    public List<ProductEntity> getAllProducts() {

        List<ProductEntity> products = productRepository.findAll();

        return products;
    }

    @Override
    public List<ProductEntity> getProductsByPagination(int pageNo, int pageSize) {
        PageRequest pageRequest = PageRequest.of(pageNo-1, pageSize);
        Page<ProductEntity> pagingProducts = productRepository.findAll(pageRequest);
        return pagingProducts.toList();
    }

    @Override
    public ProductEntity createProduct(ProductEntity productEntity) {
        if (productEntity == null) {
            throw new IllegalArgumentException("Product cannot be null");
        }
        ProductEntity savedProduct = productRepository.save(productEntity);

        return savedProduct;
    }

    @Override
    @Transactional
    public ProductEntity updateProduct(Long id, ProductEntity newProductEntity) {
        if (id == null) {
            throw new IllegalArgumentException("Product ID cannot be null");
        }
        ProductEntity product = productRepository.findById(id)
                .orElseThrow(() -> new ObjectNotFoundException("Product was not found with ID: ", id));

        product.setSku(newProductEntity.getSku());
        product.setTitle(newProductEntity.getTitle());
        product.setPrice(newProductEntity.getPrice());
        product.setDescription(newProductEntity.getDescription());

        Long productRequestCategoryId = newProductEntity.getCategory().getId();
        product.setCategory(categoryRepository.findById(productRequestCategoryId).get());

        List<String> attributeValues = newProductEntity.getAttributeValues().stream()
                .map(AttributeValueEntity::getAttributeValue)
                .collect(Collectors.toList());

        List<AttributeValueEntity> attributeValueEntityList = attributeValueRepository.findAllByAttributeValueIn(attributeValues);
        product.setAttributeValues(attributeValueEntityList);

        ProductEntity savedProduct = productRepository.save(product);

        return savedProduct;
    }


    @Override
    public void deleteProduct(Long id) {
        if (id == null) {
            throw new IllegalArgumentException("Product ID cannot be null");
        }
        try {
            productRepository.deleteById(id);
        } catch (EmptyResultDataAccessException e) {
            throw new ObjectNotFoundException("Product with ID " + id + " not found", e);
        }
    }

    @Override
    public List<ProductEntity> getProductsByCategoryAndPagination(int pageNo, int pageSize, Long categoryId) {
        PageRequest pageRequest = PageRequest.of(pageNo-1, pageSize);
        Page<ProductEntity> pagingProducts = productRepository.findByCategoryId(categoryId, pageRequest);
        return pagingProducts.toList();
    }

    @Override
    public List<ProductEntity> getProductsByPartialTitle(int pageNo, int pageSize, String partialTitle) {
        PageRequest pageRequest = PageRequest.of(pageNo - 1, pageSize);
        Page<ProductEntity> pagingProducts = productRepository.findByTitleContainingIgnoreCase(partialTitle, pageRequest);
        return pagingProducts.toList();
    }

    @Override
    public List<ProductEntity> getProductsByPartialTitleAndSort(int pageNo, int pageSize, String partialTitle, Sort.Direction sortDirection) {
        PageRequest pageRequest = PageRequest.of(pageNo - 1, pageSize, Sort.by(sortDirection, "price"));
        Page<ProductEntity> pagingProducts = productRepository.findByTitleContainingIgnoreCase(partialTitle, pageRequest);
        return pagingProducts.toList();
    }
    @Override
    public List<ProductEntity> getProductsSorted(int pageNo, int pageSize, Sort.Direction sortDirection) {
        PageRequest pageRequest = PageRequest.of(pageNo - 1, pageSize, Sort.by(sortDirection, "price"));
        Page<ProductEntity> pagingProducts = productRepository.findAll(pageRequest);
        return pagingProducts.toList();
    }

    @Transactional
    @Override
    public ProductResponse addCategoryToProduct(Long productId, Long categoryId) {
        ProductEntity product = productRepository.findById(productId)
                .orElseThrow(() -> new EntityNotFoundException("Product not found with ID: " + productId));

        CategoryEntity category = categoryRepository.findById(categoryId)
                .orElseThrow(() -> new ObjectNotFoundException("Category was not found with ID: ", categoryId));

        product.setCategory(category);

        ProductEntity updatedProduct = productRepository.save(product);

        return productMapper.productEntityToResponse(updatedProduct);
    }


}
