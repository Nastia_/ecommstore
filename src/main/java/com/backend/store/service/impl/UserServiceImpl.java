package com.backend.store.service.impl;

import com.backend.store.dto.request.UserRequest;
import com.backend.store.dto.response.UserResponse;
import com.backend.store.entity.UserEntity;
import com.backend.store.entity.WishListEntity;
import com.backend.store.mapper.UserMapper;
import com.backend.store.repository.UserRepository;
import com.backend.store.service.UserService;
import com.backend.store.service.WishListService;
import jakarta.persistence.Transient;
import org.hibernate.ObjectNotFoundException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class UserServiceImpl implements UserService {
    UserRepository userRepository;
    WishListService wishListService;

    @Autowired
    public UserServiceImpl(UserRepository userRepository,WishListService wishListService) {
        this.userRepository = userRepository;
        this.wishListService = wishListService;
    }

    @Override
    public UserEntity getUserById(Long id) {
        if (id == null) {
            throw new IllegalArgumentException("User ID cannot be null");
        }
        UserEntity userEntity = userRepository.findById(id)
                .orElseThrow(() -> new ObjectNotFoundException("User was not found with ID: ", id));

        return userEntity;
    }

    @Override
    public List<UserEntity> getAllUsers() {
        List<UserEntity> users = userRepository.findAll();

        return users;
    }

    @Transient
    @Override
    public UserEntity createUser(UserEntity userEntity) {
        if (userEntity == null) {
            throw new IllegalArgumentException("User cannot be null");
        }

        UserEntity savedUser = userRepository.save(userEntity);
        WishListEntity wishList = new WishListEntity();
        wishList.setUser(savedUser);

        WishListEntity savedWishList = wishListService.createWishList(wishList);

        savedUser.setWishList(savedWishList);
        return userRepository.save(savedUser);

    }

    @Override
    public UserEntity updateUser(Long id, UserEntity newUserEntity) {
        if (newUserEntity == null) {
            throw new IllegalArgumentException("UserRequest cannot be null");
        }

        UserEntity user = userRepository.findById(id)
                .orElseThrow(() -> new RuntimeException("User with ID " + id + " not found"));

        user.setFirstName(newUserEntity.getFirstName());
        user.setLastName(newUserEntity.getLastName());
        user.setEmail(newUserEntity.getEmail());
        user.setUserRole((newUserEntity.getUserRole()));
        user.setPassword(newUserEntity.getPassword());

        UserEntity updatedUser = userRepository.save(user);

        return updatedUser;
    }

    @Override
    public void deleteUser(Long id) {
        if (id == null) {
            throw new IllegalArgumentException("User ID cannot be null");
        }

        try {
            userRepository.deleteById(id);
        } catch (EmptyResultDataAccessException e) {
            throw new ObjectNotFoundException("User with ID " + id + " not found", e);
        }
    }

}
