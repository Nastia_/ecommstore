package com.backend.store.service.impl;

import com.backend.store.entity.ProductEntity;
import com.backend.store.entity.WishListEntity;
import com.backend.store.repository.WishListRepository;
import com.backend.store.service.ProductService;
import com.backend.store.service.WishListService;
import org.hibernate.ObjectNotFoundException;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class WishListServiceImpl implements WishListService {
    WishListRepository wishListRepository;
    ProductService productService;

    public WishListServiceImpl(WishListRepository wishListRepository, ProductService productService) {
        this.wishListRepository = wishListRepository;
        this.productService = productService;
    }

    @Override
    public WishListEntity getWishListById(Long id) {
        if (id == null) {
            throw new IllegalArgumentException("WishList value ID cannot be null");
        }
        WishListEntity wishListEntity = wishListRepository.findById(id)
                .orElseThrow(() -> new ObjectNotFoundException("WishList value was not found with ID: ", id));

        return wishListEntity;
    }

    @Override
    public WishListEntity getWihListByUserId(Long id) {
        if (id == null) {
            throw new IllegalArgumentException("WishList value ID cannot be null");
        }
        WishListEntity wishListEntity = wishListRepository.findByUserId(id);

        return wishListEntity;
    }

    @Override
    public List<WishListEntity> getAllWishLists() {
        List<WishListEntity> wishListEntities = wishListRepository.findAll();
        return wishListEntities;
    }

    @Override
    public WishListEntity createWishList(WishListEntity wishListEntity) {
        if (wishListEntity == null) {
            throw new IllegalArgumentException("WishList cannot be null");
        }
        WishListEntity savedWishList = wishListRepository.save(wishListEntity);
        return savedWishList;

    }

    @Override
    public WishListEntity updateWishList(Long id, WishListEntity wishListEntity) {
        if (id == null) {
            throw new IllegalArgumentException("WishList ID cannot be null");
        }
        WishListEntity wishList = wishListRepository.findById(id)
                .orElseThrow(() -> new ObjectNotFoundException("WishList was not found with ID: ", id));
        wishList.setUser(wishListEntity.getUser());
        wishList.setProducts(wishListEntity.getProducts());
        return wishList;
    }

    private void validateInput(Long productId, Long userId) {
        if (productId == null || userId == null) {
            throw new IllegalArgumentException("userId or productId ID cannot be null");
        }
    }

    private List<ProductEntity> getOrCreateProductsList(WishListEntity wishList) {
        List<ProductEntity> products = wishList.getProducts();
        if (products == null) {
            products = new ArrayList<>();
        }
        return products;
    }

    private WishListEntity saveWishList(WishListEntity wishList) {
        return wishListRepository.save(wishList);
    }

    @Override
    public WishListEntity addProductToTheWishList(Long productId, Long userId) {
        validateInput(productId, userId);

        WishListEntity wishList = wishListRepository.findByUserId(userId);
        ProductEntity product = productService.getProductById(productId);

        List<ProductEntity> products = getOrCreateProductsList(wishList);

        products.add(product);

        wishList.setProducts(products);

        return saveWishList(wishList);

    }

    @Override
    public WishListEntity deleteProductFromWishList(Long productId, Long userId) {
        validateInput(productId, userId);

        WishListEntity wishList = wishListRepository.findByUserId(userId);
        ProductEntity product = productService.getProductById(productId);

        List<ProductEntity> products = getOrCreateProductsList(wishList);

        products.remove(product);

        wishList.setProducts(products);

        return wishListRepository.save(wishList);
    }

    @Override
    public void deleteWishListEntity(Long id) {
        if (id == null) {
            throw new IllegalArgumentException("Product ID cannot be null");
        }
        try {
            wishListRepository.deleteById(id);
        } catch (EmptyResultDataAccessException e) {
            throw new ObjectNotFoundException("Product with ID " + id + " not found", e);
        }
    }
}
