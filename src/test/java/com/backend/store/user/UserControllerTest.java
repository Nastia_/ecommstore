package com.backend.store.user;

import com.backend.store.controller.UserController;
import com.backend.store.dto.request.UserRequest;
import com.backend.store.dto.response.UserResponse;
import com.backend.store.entity.UserEntity;
import com.backend.store.enums.UserRole;
import com.backend.store.mapper.UserMapper;
import com.backend.store.service.UserService;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.hamcrest.CoreMatchers;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.ArrayList;
import java.util.List;

import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;

@WebMvcTest(controllers = UserController.class)
@AutoConfigureMockMvc(addFilters = false)
@ExtendWith(MockitoExtension.class)
class UserControllerTest {
    @Autowired
    private MockMvc mockMvc;
    @Autowired
    private ObjectMapper objectMapper;

    @MockBean
    private UserService userService;

    @MockBean
    private UserMapper userMapper;

    private static UserRequest userRequest;
    private static UserResponse userResponse;
    private static UserEntity userEntity;
    private static UserEntity userEntitySaved;

    @BeforeAll
    public static void setUpClass() {

        userRequest = UserRequest.builder()
                .firstName("Lory")
                .lastName("Node")
                .email("lory@example.com")
                .password("123dddAXx")
                .userRole(UserRole.CUSTOMER)
                .build();

        userResponse = UserResponse.builder()
                .id(1L)
                .firstName("Lory")
                .lastName("Node")
                .email("lory@example.com")
                .userRole(UserRole.CUSTOMER)
                .build();

        userEntity = UserEntity.builder()
                .firstName("Lory")
                .lastName("Node")
                .email("lory@example.com")
                .userRole(UserRole.CUSTOMER)
                .build();
        userEntitySaved = UserEntity.builder()
                .firstName("Lory")
                .lastName("Node")
                .email("lory@example.com")
                .userRole(UserRole.CUSTOMER)
                .build();
    }

    @Test
    public void UserController_CreateUser_ReturnCreated() throws Exception {
        when(userMapper.userRequestToEntity(any(UserRequest.class))).thenReturn(userEntity);
        when(userService.createUser(any(UserEntity.class))).thenReturn(userEntitySaved);
        when(userMapper.userEntityToResponse(userEntitySaved)).thenReturn(userResponse);

        mockMvc.perform(post("/api/users")
                        .contentType(MediaType.APPLICATION_JSON)
                        .content(objectMapper.writeValueAsString(userRequest)))

                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.jsonPath("$.id").exists());
    }


    @Test
    public void UserController_getUserById_ReturnOkUserResponse() throws Exception {
        Long userId = 1L;

        when(userService.getUserById(userId)).thenReturn(userEntity);
        when(userMapper.userEntityToResponse(userEntity)).thenReturn(userResponse);

        ResultActions response = mockMvc.perform(get("/api/users/{id}", userId)
                .contentType(MediaType.APPLICATION_JSON));

        response.andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.jsonPath("$.firstName", CoreMatchers.is(userResponse.getFirstName())))
                .andExpect(MockMvcResultMatchers.jsonPath("$.lastName", CoreMatchers.is(userResponse.getLastName())));
    }

    @Test
    public void UserController_getAllUsers_ReturnOkUserResponse() throws Exception {
        List<UserEntity> userEntityList = new ArrayList<>();
        userEntityList.add(userEntity);

        when(userService.getAllUsers()).thenReturn(userEntityList);
        when(userMapper.userEntityToResponse(userEntity)).thenReturn(userResponse);


        ResultActions response = mockMvc.perform(get("/api/users")
                .contentType(MediaType.APPLICATION_JSON));

        response.andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].firstName", CoreMatchers.is(userResponse.getFirstName())))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].lastName", CoreMatchers.is(userResponse.getLastName())))
                .andExpect(MockMvcResultMatchers.jsonPath("$[0].email", CoreMatchers.is(userResponse.getEmail())));
    }


    @Test
    public void UserController_UpdateUser_ReturnOkUserResponse() throws Exception {
        Long userId = 1L;

        when(userMapper.userRequestToEntity(any(UserRequest.class))).thenReturn(userEntity);
        when(userService.updateUser(any(Long.class), any(UserEntity.class))).thenReturn(userEntitySaved);
        when(userMapper.userEntityToResponse(userEntitySaved)).thenReturn(userResponse);


        ResultActions response = mockMvc.perform(put("/api/users/{id}", userId)
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(userRequest)));

        response.andExpect(MockMvcResultMatchers.status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON));
    }


    @Test
    public void UserController_DeleteUser_ReturnString() throws Exception {
        Long userId = 1L;

        doNothing().when(userService).deleteUser(userId);

        ResultActions response = mockMvc.perform(delete("/api/users/{id}", userId)
                .contentType(MediaType.APPLICATION_JSON));

        response.andExpect(MockMvcResultMatchers.status().isNoContent());
    }

}
