package com.backend.store.user;

import com.backend.store.entity.UserEntity;
import com.backend.store.repository.UserRepository;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.jdbc.EmbeddedDatabaseConnection;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import java.util.List;
import java.util.Optional;

import static com.backend.store.enums.UserRole.ADMIN;
import static com.backend.store.enums.UserRole.CUSTOMER;

@DataJpaTest
@AutoConfigureTestDatabase(connection = EmbeddedDatabaseConnection.H2)
public class UserRepositoryTest {
    @Autowired
    private UserRepository userRepository;
    private static UserEntity user;
    private static UserEntity user1;
    private static UserEntity user2;

    @BeforeAll
    public static void setUpClass() {
        user = UserEntity.builder()
                .firstName("Lory")
                .lastName("Node")
                .email("lory@example.com")
                .password("123dddAXx")
                .userRole(CUSTOMER)
                .build();

        user1 = UserEntity.builder()
                .firstName("Lory1")
                .lastName("Node1")
                .email("lory@example.com")
                .password("123dddAXx")
                .userRole(CUSTOMER)
                .build();

        user2 = UserEntity.builder()
                .firstName("Lory2")
                .lastName("Node2")
                .email("lor2y@example.com")
                .password("123dddAXx")
                .userRole(CUSTOMER)
                .build();
    }

    @Test
    public void UserRepository_CreateUser_ReturnsSavedUser() {
        UserEntity savedUser = userRepository.save(user);

        assertThat(savedUser).isNotNull();
        assertThat(savedUser.getId()).isGreaterThan(0);
    }

    @Test
    public void UserRepository_GetAll_ReturnsListOfUsers() {
        userRepository.save(user1);
        userRepository.save(user2);

        List<UserEntity> usersList = userRepository.findAll();

        assertThat(usersList).isNotNull();
        assertThat(usersList.size()).isEqualTo(2);

    }

    @Test
    public void UserRepository_FindById_ReturnsUser() {
        userRepository.save(user);

        UserEntity resultUser = userRepository.findById(user.getId()).get();

        assertThat(resultUser).isNotNull();
        assertThat(resultUser.getId()).isEqualTo(user.getId());
        assertThat(resultUser.getFirstName()).isEqualTo(user.getFirstName());
        assertThat(resultUser.getLastName()).isEqualTo(user.getLastName());
        assertThat(resultUser.getEmail()).isEqualTo(user.getEmail());
        assertThat(resultUser.getPassword()).isEqualTo(user.getPassword());
        assertThat(resultUser.getUserRole()).isEqualTo(user.getUserRole());
    }

    @Test
    public void UserRepository_UpdateUser_ReturnUserNotNull() {
        userRepository.save(user);

        UserEntity userSave = userRepository.findById(user.getId()).get();
        userSave.setFirstName("Lory2");
        userSave.setLastName("Node2");
        userSave.setEmail("lory3@example.com");
        userSave.setPassword("123fdfdAs");
        userSave.setUserRole(ADMIN);

        UserEntity updatedUser = userRepository.save(userSave);

        assertThat(updatedUser).isNotNull();
        assertThat(updatedUser.getId()).isEqualTo(user.getId());
        assertThat(updatedUser.getFirstName()).isEqualTo("Lory2");
        assertThat(updatedUser.getLastName()).isEqualTo("Node2");
        assertThat(updatedUser.getEmail()).isEqualTo("lory3@example.com");
        assertThat(updatedUser.getPassword()).isEqualTo("123fdfdAs");
        assertThat(updatedUser.getUserRole()).isEqualTo(ADMIN);
    }

    @Test
    public void UserRepository_DeleteUser_ReturnUserIsEmpty() {
        UserEntity user = UserEntity.builder()
                .firstName("Lory1")
                .lastName("Node1")
                .email("lory@example.com")
                .password("123dddAXx")
                .userRole(CUSTOMER)
                .build();

        userRepository.save(user);

        userRepository.deleteById(user.getId());
        Optional<UserEntity> userReturn = userRepository.findById(user.getId());

        assertThat(userReturn).isEmpty();
    }


}
