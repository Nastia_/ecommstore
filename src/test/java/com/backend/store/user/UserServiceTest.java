package com.backend.store.user;

import com.backend.store.dto.request.UserRequest;
import com.backend.store.entity.UserEntity;
import com.backend.store.enums.UserRole;
import com.backend.store.mapper.UserMapper;
import com.backend.store.repository.UserRepository;
import com.backend.store.service.impl.UserServiceImpl;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.*;
import org.mockito.junit.jupiter.MockitoExtension;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertAll;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
public class UserServiceTest {
    @Mock
    private UserRepository userRepository;
    @Spy
    private UserMapper userMapper;
    @InjectMocks
    private UserServiceImpl userService;

    private static UserEntity user;
    private static UserRequest userRequest;

    @BeforeAll
    public static void setUpClass() {
        user = UserEntity.builder()
                .firstName("Lory")
                .lastName("Node")
                .email("lory@example.com")
                .password("123dddAXx")
                .userRole(UserRole.CUSTOMER)
                .build();

        userRequest = UserRequest.builder()
                .firstName("Lory")
                .lastName("Node")
                .email("lory@example.com")
                .password("123dddAXx")
                .userRole(UserRole.CUSTOMER)
                .build();
    }

    @Test
    public void UserService_CreateUser_ReturnUserResponse() {

        when(userRepository.save(Mockito.any(UserEntity.class))).thenReturn(user);

        UserEntity userEntity = userService.createUser(user);

        assertThat(userEntity).isNotNull();
        assertThat(userEntity.getFirstName()).isEqualTo("Lory");
    }

    @Test
    public void UserService_GetAllUsers_ReturnUserResponseList() {
        List<UserEntity> userList = new ArrayList<>();
        userList.add(user);

        when(userRepository.findAll()).thenReturn(userList);

        List<UserEntity> userEntities = userService.getAllUsers();

        assertThat(userEntities).isNotNull();
        assertThat(userEntities.size()).isEqualTo(1);
    }

    @Test
    public void UserService_GetUserById_ReturnUserResponse() {
        Long userId = 1L;

        when(userRepository.findById(userId)).thenReturn(Optional.ofNullable(user));

        UserEntity userEntity = userService.getUserById(userId);

        assertThat(userEntity).isNotNull();
    }

    @Test
    public void UserService_UpdateUserById_ReturnUserResponse() {
        Long userId = 1L;

        when(userRepository.findById(userId)).thenReturn(Optional.ofNullable(user));
        when(userRepository.save(user)).thenReturn(user);

        UserEntity userEntity = userService.updateUser(1L, user);

        assertThat(userEntity).isNotNull();
        assertThat(userEntity.getFirstName()).isEqualTo("Lory");

        verify(userRepository, times(1)).findById(userId);
        verify(userRepository, times(1)).save(user);
    }

    @Test
    public void UserService_DeleteUserById_ReturnVoid() {
        Long userId = 1L;

        doNothing().when(userRepository).deleteById(userId);

        assertAll(() -> userService.deleteUser(userId));
    }


}